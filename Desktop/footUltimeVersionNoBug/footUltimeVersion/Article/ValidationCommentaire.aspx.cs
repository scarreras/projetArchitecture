﻿using projetfoot.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace footUltimeVersion.Article
{
    public partial class ValidationCommentaire : System.Web.UI.Page
    {
        List<string[]> commentaires;
        CommentaireManager commentaireManager = new CommentaireManager();

        protected void Page_Init(object sender, EventArgs e)
        {
            commentaires = commentaireManager.recupererComEnAttenteDeValidation();

            if (commentaires.Count == 0)
            {
                afficherMessageAucunCom();
            }
            else
            {
                genererListeCommentaires();
            }
        }


        private void genererListeCommentaires()
        {
            for(int i=0; i<commentaires.Count; i++)
            {
                //On récupère le booléen pour savoir si le commentaire à déjà été validé ou non
                bool valide = Convert.ToBoolean(commentaires.ElementAt(i)[2]);

                if (valide == false)
                {
                    ListItem checkbox = new ListItem();
                    checkbox.Text = commentaires.ElementAt(i)[1];
                    checkbox.Attributes["value"] = commentaires.ElementAt(i)[0];
                    
                    CheckBoxCommentaire.Items.Add(checkbox);
                    
                }
            }
        }

        /*S'il n'y a plus aucun commentaire à trier on en informe l'utilisateur */
        private void afficherMessageAucunCom()
        {
            HtmlGenericControl messageCom = new HtmlGenericControl("p");
            messageCom.InnerHtml = "Il n'y a plus de commentaire à trier";
            message_utilisateur.Controls.Add(messageCom);
        }



        protected void Button1_Click(object sender, EventArgs e)
        {
            /*Pour chaque box on regarde ce que l'utilisateur à selectionné */
            foreach(ListItem box in CheckBoxCommentaire.Items)
            {

                int id_commentaire = Convert.ToInt32(box.Attributes["value"]);

                /*S'il valide le commentaire on change la valeur du booléen, sinon on supprime définitivement le commentaire */
                if (box.Selected == true)
                {
                    
                    commentaireManager.valider_commentaire(id_commentaire);
                }
                else
                {
                    commentaireManager.supprimer_commentaire(id_commentaire);
                }
                
            }

            
            Page.Response.Redirect(Page.Request.Url.ToString(), true);


        }

       
    }
}