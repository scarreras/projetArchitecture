﻿using projetfoot.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace footUltimeVersion.Article
{
    public partial class ListeArticles2 : System.Web.UI.Page
    {
        ArticleManager article = new ArticleManager();
        List<string[]> liste_articles = new List<string[]>();

        protected void Page_Load(object sender, EventArgs e)
        {
            liste_articles = article.recupererListeArticle();
            genererListeArticles();
        }

        private void genererListeArticles()
        {
            for (int i = 0; i < liste_articles.Count; i++)
            {
               
                    
                genererArticle(liste_articles.ElementAt(i));

                
            }
        }

        private void genererArticle(string[] article)
        {
            string titre_article = article[0];
            string description_article = article[1];
            string auteur_article = article[2];
            string id_art = article[3];
            string image_article = article[4];

            HtmlGenericControl div = genererDiv();
            HtmlGenericControl titre = genererTitre(titre_article);
            HtmlGenericControl description = genererDescription(description_article);
            HtmlGenericControl bouton = genererBoutonLien("../Article/AfficherArticle?id="+id_art);
            HtmlGenericControl paragraphe = new HtmlGenericControl("p");

            paragraphe.Controls.Add(titre);
            paragraphe.Controls.Add(description);
            paragraphe.Controls.Add(bouton);
           
            div.Controls.Add(paragraphe);
            
            div.Attributes["style"] = "background-image:url(" + image_article + "); background-size: 384px 216px;background-repeat: no-repeat;margin-top: 5px; ";
            

            ligne_article.Controls.Add(div);
        }

        private HtmlGenericControl genererTitre(string titre_article)
        {
            HtmlGenericControl titre_article_accueil = new HtmlGenericControl("h2");
            titre_article_accueil.InnerHtml = titre_article;
            titre_article_accueil.Attributes["style"]= " background-color: rgba(255, 255, 255, 0.7);margin-right: 20px;margin-bottom:0px;";

            return titre_article_accueil;
        }

        private HtmlGenericControl genererDescription(string description)
        {
            HtmlGenericControl description_article = new HtmlGenericControl("p");
            description_article.InnerHtml = description;
            description_article.Attributes["style"] = "background-color: rgba(255, 255, 255, 0.7);margin-right:20px;";

            return description_article;
        }

        /*Création de la DIv contenant l'article */
        private HtmlGenericControl genererDiv()
        {
            HtmlGenericControl div_article = new HtmlGenericControl("div");
            div_article.Attributes["class"] = "col-md-4";


            return div_article;
        }

        private HtmlGenericControl genererBoutonLien(string lien_article)
        {
            HtmlGenericControl paragraphe = new HtmlGenericControl("p");
            HtmlGenericControl lien = new HtmlGenericControl("a");
            lien.Attributes["class"] = "btn btn-default";
            lien.Attributes["href"] = lien_article;
            lien.InnerHtml = "Lire l'article";

            paragraphe.Controls.Add(lien);

            return paragraphe;
        }

        



    }
}
