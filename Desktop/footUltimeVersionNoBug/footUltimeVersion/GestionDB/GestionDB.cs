﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projetfoot.GestionDB
{
    /* Classe contenant les méthodes pour intéragir avec la BDD */
    public class GestionDB
    {
        /* Fonction retournant les identifiants pour se connecter à la base */
        public string connectDatabase()
        {
            //Contient les identifiants pour se connecter à la base de données
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;database=projetfoot;";

            return connectionString;
        }


        /*Il existe 3 types de requêtes de base: 
         *   - celles qui exécute une action sur la base (UPDATE, DELETE, INSERT...)
         *   - celles qui vont récupérer des données dans la base (SELECT)
         *   - celles qui vérifie l'existence de données dans la base
        */


        /* Méthode lançant une requête vers la base */
        public void request(string query) //Prend la requête à soumettre en paramètre
        {
            MySqlConnection databaseConnection = new MySqlConnection(connectDatabase()); //Lance la connexion
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;


            try
            {
                databaseConnection.Open();
                MySqlDataReader reader = commandDatabase.ExecuteReader();

                databaseConnection.Close(); //On coupe la connexion avec la base
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


        }

        /* Méthode permettant de récupérer le contenu d'une requête SELECT */
        public List<string[]> selectRequest(string query)
        {
            List<string[]> resultRequest = new List<string[]>();//On déclare une liste pour stocker les résultats

            MySqlConnection databaseConnection = new MySqlConnection(connectDatabase());
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;

            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();

                // Si des lignes sont disponible
                if (reader.HasRows)
                {


                    while (reader.Read())
                    {
                        string[] fields = new string[reader.FieldCount];

                        //On ajoute tout les champs demandés
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            if (reader.IsDBNull(i))
                            {
                                fields[i] = "";
                            }
                            else
                            {
                                fields[i] = reader.GetString(i);
                            }
                            //Console.WriteLine(reader.GetString(0));//Ligne test
                        }

                        resultRequest.Add(fields);
                    }


                }
                else
                {
                    Console.WriteLine("No rows found.");
                }

                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            return resultRequest;
        }

        /* Méthode permettant de savoir si un ensemble de données est présent dans la base */
        public bool checkRequest(string query)
        {
            bool check = false;//On initialise à false le booléon de vérification

            MySqlConnection databaseConnection = new MySqlConnection(connectDatabase());
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;

            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();

                if (reader.HasRows)
                {
                    check = true; //Si la requête fonctionne on change la valeur du booléen
                    Console.WriteLine("Les données sont présentes"); //Ligne de test
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }

                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return check;

        }

        //Requête permettant de récupérer le dernier ID inséré + d'exécuter la requête
        public long requeteADI(string query) //Prend la requête à soumettre en paramètre
        {
            long derniere_insertion = 0;

            MySqlConnection databaseConnection = new MySqlConnection(connectDatabase()); //Lance la connexion
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);

            commandDatabase.CommandTimeout = 60;

            try
            {
                databaseConnection.Open();
                MySqlDataReader reader = commandDatabase.ExecuteReader();
                derniere_insertion = commandDatabase.LastInsertedId;
                databaseConnection.Close(); //On coupe la connexion avec la base
            }
            catch (Exception ex)
            {
                
            }

            return derniere_insertion;
        }



    }

}