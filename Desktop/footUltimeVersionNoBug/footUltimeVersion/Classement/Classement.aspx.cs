﻿using projetfoot.GestionDB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace footUltimeVersion
{
   
        public partial class Classement : System.Web.UI.Page
        {
            GestionDB query = new GestionDB();
            string id_champ = "";

            public Classement()
            {
                
            }
            public Classement(int id_champ)
            {
                
                
            }
            
            protected void Page_Load(object sender, EventArgs e)
            {
                this.id_champ = Request["id"];
                DataTable dt = new DataTable();
                DataRow dr = null;
                GridView1.Columns[0].HeaderText = "Rang";
                dt.Columns.Add(new DataColumn("Equipe", typeof(string)));
                dt.Columns.Add(new DataColumn("J", typeof(int)));
                dt.Columns.Add(new DataColumn("G", typeof(int)));
                dt.Columns.Add(new DataColumn("N", typeof(int)));
                dt.Columns.Add(new DataColumn("P", typeof(int)));
                dt.Columns.Add(new DataColumn("BP", typeof(int)));
                dt.Columns.Add(new DataColumn("BC", typeof(int)));
                dt.Columns.Add(new DataColumn("Diff", typeof(int)));
                dt.Columns.Add(new DataColumn("PTS", typeof(int)));

                DataView dv = dt.DefaultView;

                dv.Sort = "PTS DESC, Diff DESC, BP DESC";

                List<string[]> teamsId = query.selectRequest(@"SELECT id_equip 
                                                        FROM EQUIPE 
                                                        WHERE id_champ = '" + id_champ + "';");


                for (int i = 0; i < teamsId.Count; i++)
                {

                    EquipeFootball team = new EquipeFootball(Convert.ToInt32(teamsId.ElementAt(i)[0]));

                    string name = team.teamName();
                    string games = team.gamesPlayed();
                    int[] stats = team.statistics();
                    int[] goals = team.teamGoals();

                    dr = dt.NewRow();
                    dr["Equipe"] = name;
                    dr["J"] = games;
                    dr["G"] = stats.ElementAt(0);
                    dr["N"] = stats.ElementAt(1);
                    dr["P"] = stats.ElementAt(2);
                    dr["BP"] = goals.ElementAt(0);
                    dr["BC"] = goals.ElementAt(1);
                    dr["Diff"] = goals.ElementAt(2);
                    dr["PTS"] = stats.ElementAt(3);
                    dt.Rows.Add(dr);

                }

                ViewState["CurrentTable"] = dt;

                GridView1.DataSource = dt;
                GridView1.DataBind();
            }


        }
    }
