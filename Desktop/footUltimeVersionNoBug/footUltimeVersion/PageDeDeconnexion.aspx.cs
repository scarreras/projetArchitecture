﻿using projetfoot.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace footUltimeVersion
{
    public partial class PageDeDeconnexion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionManager.Id_utilisateur = null;
            SessionManager.Login = null;
        }
    }
}