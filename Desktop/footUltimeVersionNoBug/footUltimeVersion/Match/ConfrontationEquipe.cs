﻿using projetfoot.GestionDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace footUltimeVersion
{
    public class ConfrontationEquipe
    {
        GestionDB query = new GestionDB();

        int id;
        string date;
        int id_journee;

        public ConfrontationEquipe(int id)
        {
            this.id = id;
        }

        public int confrontation()
        {
            List<string[]> conf = query.selectRequest(@"SELECT buts_inscrits, id_equip
                                                      FROM EQUIPE_CONFRONTATION
                                                      WHERE id_confront = '" + id + "'");

            int whowon = 0;
            int team1goals = Convert.ToInt32(conf.ElementAt(0)[0]);
            int team2goals = Convert.ToInt32(conf.ElementAt(1)[0]);

            if (team1goals > team2goals)
            {
                whowon = Convert.ToInt32(conf.ElementAt(0)[1]);
            }
            else if (team2goals > team1goals)
            {
                whowon = Convert.ToInt32(conf.ElementAt(1)[1]);
            }
            else if (team1goals == team2goals)
            {
                whowon = 0;
            }
            return whowon;
        }

        public int goalsAgainst(int teamId)
        {
            List<string[]> goalsA = query.selectRequest(@"SELECT buts_inscrits, id_equip
                                                      FROM EQUIPE_CONFRONTATION
                                                      WHERE id_confront = '" + id + "' AND " +
                                                      "id_equip <> '" + teamId + "'");


            return Convert.ToInt32(goalsA.ElementAt(0)[0]);
        }
    }
}
