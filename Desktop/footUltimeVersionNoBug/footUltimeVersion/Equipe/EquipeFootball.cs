﻿using projetfoot.GestionDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace footUltimeVersion
{
    public class EquipeFootball
    {
        GestionDB query = new GestionDB();

        int id;

        public EquipeFootball(int id)
        {
            this.id = id;
        }

        public string teamName()
        {
            List<string[]> team = query.selectRequest(@"SELECT nom_equipe
                                                            FROM EQUIPE
                                                            WHERE id_equip = '" + id + "'");

            return team.ElementAt(0)[0];
        }

        public string gamesPlayed()
        {
            List<string[]> listgamesplayed = query.selectRequest(@"SELECT COUNT(id_confront)
                                                                FROM EQUIPE_CONFRONTATION
                                                                WHERE id_equip = '" + id + "'");

            return listgamesplayed.ElementAt(0)[0];
        }

        public int[] statistics()
        {

            List<string[]> listmatches = query.selectRequest(@"SELECT id_confront
                                                             FROM EQUIPE_CONFRONTATION
                                                             WHERE id_equip = '" + id + "'");

            int wincount = 0;
            int drawcount = 0;
            int losscount = 0;
            int points = 0;

            for (int i = 0; i < listmatches.Count; i++)
            {
                int id_match = Convert.ToInt32(listmatches.ElementAt(i)[0]);
                ConfrontationEquipe match = new ConfrontationEquipe(id_match);
                int results = match.confrontation();

                if (results == id)
                {
                    wincount = wincount + 1;
                    points = points + 3;
                }
                else if (results == 0)
                {
                    drawcount = drawcount + 1;
                    points = points + 1;
                }
                else
                {
                    losscount = losscount + 1;
                }

            }

            int[] stats = new int[] { wincount, drawcount, losscount, points };


            return stats;
        }

        public int[] teamGoals()
        {
            List<string[]> listgoalsfor = query.selectRequest(@"SELECT SUM(buts_inscrits)
                                                                FROM EQUIPE_CONFRONTATION
                                                                WHERE id_equip = '" + id + "'");

            List<string[]> listmatches = query.selectRequest(@"SELECT id_confront
                                                             FROM EQUIPE_CONFRONTATION
                                                             WHERE id_equip = '" + id + "'");

            int goalsagainst = 0;
            for (int i = 0; i < listmatches.Count; i++)
            {
                int id_match = Convert.ToInt32(listmatches.ElementAt(i)[0]);
                ConfrontationEquipe match = new ConfrontationEquipe(id_match);
                goalsagainst += match.goalsAgainst(id);

            }

            int goalsfor = Convert.ToInt32(listgoalsfor.ElementAt(0)[0]);

            int goalDiff = 0;

            if (goalsfor > goalsagainst)
            {
                goalDiff = goalsfor - goalsagainst;
            }
            else if (goalsfor == goalsagainst)
            {
                goalDiff = 0;
            }
            else if (goalsfor < goalsagainst)
            {
                goalDiff = goalsagainst - goalsfor;
                goalDiff = -Math.Abs(goalDiff);
            }

            int[] goals = new[] { goalsfor, goalsagainst, goalDiff };

            return goals;
        }

        public List<string[]> teamInfo()
        {
            List<string[]> infolist = query.selectRequest(@"SELECT *
                                                           FROM EQUIPE
                                                           WHERE id_equip = '" + id + "'");

            return infolist;
        }

        public string teamCountry()
        {
            List<string[]> leaguelist = query.selectRequest(@"SELECT nom_pays
                                                             FROM PAYS P, CHAMPIONAT C, EQUIPE E
                                                             WHERE P.id_pays = C.id_pays
                                                             AND C.id_champ = E.id_champ
                                                             AND E.id_equip = '" + id + "'");

            string league = leaguelist.ElementAt(0)[0];
            return league;

        }

    }
}