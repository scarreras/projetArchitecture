﻿using footUltimeVersion.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace footUltimeVersion.Equipe
{
    public partial class ModifierEquipe : System.Web.UI.Page
    {
        int id = 1;
        EquipeManager equipe = new EquipeManager();
        string nom;
        string annee_fondation;
        string entreneur;
        string stade;
        string capacite_stade;
        string championat;

        protected void Page_Load(object sender, EventArgs e)
        {
            string champ = afficherInfoEquipe();
            recupererChampionats(champ);

        }

        private string afficherInfoEquipe()
        {
            List<string[]> infos = equipe.recupererInfoEquipe(id);

            System.Diagnostics.Debug.WriteLine(infos);
            
            textbox_nom.Text = infos.ElementAt(0)[0];
            textbox_annee.Text = infos.ElementAt(0)[1];
            textbox_entreneur.Text = infos.ElementAt(0)[2];
            textbox_stade.Text = infos.ElementAt(0)[3];
            textbox_capacite.Text = infos.ElementAt(0)[4];
            dropdownlist_championat.Items.Add(new ListItem(infos.ElementAt(0)[5]));

            return infos.ElementAt(0)[5];
        }

        protected void button_valider(object sender, EventArgs e)
        {
            nom = textbox_nom.Text;
            annee_fondation = textbox_annee.Text;
            entreneur = textbox_entreneur.Text;
            stade = textbox_stade.Text;
            capacite_stade = textbox_capacite.Text;
            championat = dropdownlist_championat.Text;

            equipe.modifierEquipe(id, nom, annee_fondation, entreneur, stade, capacite_stade, championat);
        }

        private void recupererChampionats(string champ)
        {
            List<string[]> liste_championats = equipe.recupererListeChampionats();

            for (int i = 0; i < liste_championats.Count; i++)
            {
                dropdownlist_championat.Items.Add(new ListItem(liste_championats.ElementAt(i)[0].ToString()));
            }

            dropdownlist_championat.Items.Remove(dropdownlist_championat.Items.FindByText(champ));
        }


    }
}