﻿using projetfoot.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace footUltimeVersion
{
    public partial class _Default : Page
    {
        ArticleManager article = new ArticleManager();
        List<string[]> articles_accueil = new List<string[]>();

        protected void Page_Load(object sender, EventArgs e)
        {
            articles_accueil = article.recupererListeArticleAccueil();
            genererArticleEnTete(articles_accueil.ElementAt(0));
            genererListeArticleAccueil();
        }

        private void genererListeArticleAccueil()
        {
            //La boucle commence à 1 parce que l'article 0 est l'article en en tête
            for(int i = 1; i<articles_accueil.Count; i++)
            {
                genererListeArticles(articles_accueil.ElementAt(i));
            }
        }



        private void genererArticleEnTete(string[]article)
        {
            string titre_article = article[0];
            string description_article = article[1];
            string auteur_article = article[2];
            string image_article = article[4];
            string id_art = article[3];
            
            HtmlGenericControl en_tete = genererTitreEnTete(titre_article);
            HtmlGenericControl description = genererDescriptionEnTete(description_article);
            HtmlGenericControl bouton = genererBoutonEnTete("../Article/AfficherArticle?id=" + id_art);

            entete.Attributes["style"] = "background-image:url("+image_article+")";
           

            entete.Controls.Add(en_tete);
            entete.Controls.Add(description);
            entete.Controls.Add(bouton);

            

        }

        private HtmlGenericControl genererBoutonEnTete(string lien)
        {
            HtmlGenericControl paragraphe = new HtmlGenericControl("p");
            HtmlGenericControl bouton = new HtmlGenericControl("a");
            bouton.Attributes["class"] = "btn btn-primary btn-lg";
            bouton.Attributes["href"] = lien;
            bouton.InnerHtml = "Lire l'article";

            paragraphe.Controls.Add(bouton);


            return paragraphe;
        }

        private HtmlGenericControl genererDescriptionEnTete(string description)
        {
            HtmlGenericControl description_entete = new HtmlGenericControl("p");
            description_entete.InnerHtml = description;
            description_entete.Attributes["class"] = "lead";

            return description_entete;
        }

        private HtmlGenericControl genererTitreEnTete(string titre)
        {
            HtmlGenericControl titre_enTete = new HtmlGenericControl("h1");
            titre_enTete.InnerHtml = titre;

            return titre_enTete;
        }

        private void genererListeArticles(string[] article)
        {
            string titre_article = article[0];
            string description_article = article[1];
            string auteur_article = article[2];
            string image_article = article[4];
            string id_art = article[3];

            HtmlGenericControl div = genererDiv();
            HtmlGenericControl titre = genererTitre(titre_article);
            HtmlGenericControl description = genererDescription(description_article);
            HtmlGenericControl bouton = genererBoutonLien("../Article/AfficherArticle?id=" + id_art);

            div.Controls.Add(titre);
            div.Controls.Add(description);
            div.Controls.Add(bouton);
            ligne_article.Controls.Add(div);
        }

        private HtmlGenericControl genererTitre(string titre_article)
        {
            HtmlGenericControl titre_article_accueil= new HtmlGenericControl("h2");
            titre_article_accueil.InnerHtml = titre_article;

            return titre_article_accueil;
        }

        private HtmlGenericControl genererDescription(string description)
        {
            HtmlGenericControl description_article = new HtmlGenericControl("p");
            description_article.InnerHtml = description;

            return description_article;
        }

        /*Création de la DIv contenant l'article */
        private HtmlGenericControl genererDiv()
        {
            HtmlGenericControl div_article = new HtmlGenericControl("div");
            div_article.Attributes["class"] = "col-md-4";

            return div_article;
        }

        private HtmlGenericControl genererBoutonLien(string lien_article)
        {
            HtmlGenericControl paragraphe = new HtmlGenericControl("p");
            HtmlGenericControl lien = new HtmlGenericControl("a");
            lien.Attributes["class"] = "btn btn-default";
            lien.Attributes["href"] = lien_article;
            lien.InnerHtml = "Lire l'article";

            paragraphe.Controls.Add(lien);

            return paragraphe;
        }


        }
}