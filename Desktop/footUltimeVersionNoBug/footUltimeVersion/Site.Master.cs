﻿using projetfoot.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace footUltimeVersion
{
    public partial class SiteMaster : MasterPage
    {

        UtilisateurManager utilisateur = new UtilisateurManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            bool est_admin = utilisateur.verifierAdmin(SessionManager.Id_utilisateur);

            if(SessionManager.Login != null && est_admin == true)
            {
                afficherDeconnexionLogin();
                afficherLogin();
                afficherValidationCommentaire();
            }
            else if(SessionManager.Login != null)
            {
                afficherDeconnexionLogin();
                afficherLogin();
                menu_ajout.Attributes["style"] = "display:none";
                menu_modification.Attributes["style"] = "display:none";
            }
            else
            {
                afficherConnexionInscription();
                menu_ajout.Attributes["style"] = "display:none";
                menu_modification.Attributes["style"] = "display:none";
            }
        }

        //Si la variable static id_util n'est pas vide on enlève l'onglet connexiion et inscription + affichage bouton deconnexion + affichage login
        private void afficherDeconnexionLogin()
        {
            HtmlGenericControl ongletDeconnexion = new HtmlGenericControl("a");
            HtmlGenericControl baliseB = new HtmlGenericControl("b");

            ongletDeconnexion.Attributes["style"] = "color:white";
            ongletDeconnexion.Attributes["href"] = "../PageDeDeconnexion";

            baliseB.InnerHtml = "Déconnexion";
            ongletDeconnexion.Controls.Add(baliseB);

            ongletConnexion.Controls.Add(ongletDeconnexion);

        }

        private void afficherConnexionInscription()
        {
            HtmlGenericControl ongletDeConnexion = new HtmlGenericControl("a");
            HtmlGenericControl ongletDInscription = new HtmlGenericControl("a");

           

            HtmlGenericControl baliseB = new HtmlGenericControl("b");
            HtmlGenericControl baliseB2 = new HtmlGenericControl("b");

            baliseB.Attributes["style"] = "color:white;";
            ongletDeConnexion.Attributes["href"] = "../PageDeConnexion.aspx";

            baliseB2.Attributes["style"] = "color:white;";
            ongletDInscription.Attributes["href"] = "../PageInscription";

            baliseB.InnerHtml = "Connexion";
            baliseB2.InnerHtml = "Inscription";

            ongletDeConnexion.Controls.Add(baliseB);
            ongletDInscription.Controls.Add(baliseB2);

            ongletConnexion.Controls.Add(ongletDeConnexion);
            ongletInscription.Controls.Add(ongletDInscription);
        }

        private void afficherLogin()
        {
            loginUtilisateur.InnerHtml = SessionManager.Login;
        }

        private void afficherValidationCommentaire()
        {
            HtmlGenericControl validationCommentaire = new HtmlGenericControl("a");
            HtmlGenericControl baliseB = new HtmlGenericControl("b");

            validationCommentaire.Attributes["style"] = "color:white";
            validationCommentaire.Attributes["href"] = "../Article/ValidationCommentaire.aspx";

            baliseB.InnerHtml = "Commentaires";
            validationCommentaire.Controls.Add(baliseB);

            ongletConnexion.Controls.Add(validationCommentaire);

        }



    }
}