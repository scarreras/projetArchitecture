﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projetfoot.Manager
{
    public class ArticleManager
    {
        GestionDB.GestionDB req = new GestionDB.GestionDB();

        public void ajouterArticle(string titre_article, string texte_article, string description_article)
        {
            titre_article = titre_article.Replace("'", @"\'");
            texte_article = texte_article.Replace("'", @"\'");
            description_article = description_article.Replace("'", @"\'");

            req.request(@"INSERT INTO `article` (`id_art`, `titre_art`, `contenu_art`, `description`, `id_util`) VALUES(NULL, '"+titre_article+"', '"+texte_article+"', '"+description_article+"', '2');");

            
        }

        public List<string[]> recupererListeArticle()
        {
            List<string[]> liste_articles = req.selectRequest(@"SELECT titre_art, description, contenu_art, id_art, image FROM article");

            return liste_articles;
        }

        public List<string[]> recupererListeArticleAccueil()
        {
            List<string[]> liste_articles = req.selectRequest(@"SELECT titre_art, description, contenu_art, id_art, image FROM article LIMIT 4");

            return liste_articles;
        }

        public List<string[]> recupererUnArticle(string id_art)
        {
            List<string[]> article = req.selectRequest(@"SELECT titre_art, contenu_art
                                                        FROM ARTICLE 
                                                        WHERE id_art = '" + id_art + "';");

            return article;
        }

    }
}