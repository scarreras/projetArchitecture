﻿using projetfoot.GestionDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace footUltimeVersion.Manager
{
    public class ConfrontationManager
    {
        GestionDB req = new GestionDB();

        public List<string[]> recupererJournees()
        {
            List<string[]> liste_journees = req.selectRequest("SELECT nb_jour FROM JOURNEE");

            return liste_journees;
        }

        public long ajouterConfrontation(string date, string journee)
        {
            List<string[]> idJournee = req.selectRequest(@"SELECT id_jour
                                                        FROM JOURNEE
                                                        WHERE nb_jour = '" + journee + "'");

            string idJour = idJournee.ElementAt(0)[0];

            long lastId = req.requeteADI(@"INSERT INTO CONFRONTATION
                        VALUES(NULL, '" + date + "', '" + idJour + "')");

            return lastId;
        }

        public List<string[]> recupererEquipes()
        {
            List<string[]> liste_equipes = req.selectRequest(@"SELECT nom_equipe
                                                            FROM EQUIPE");

            return liste_equipes;
        }

        public void ajouterEquipe1(string equipe, int idConf)
        {
            List<string[]> idEquipe = req.selectRequest(@"SELECT id_equip
                                                        FROM EQUIPE
                                                        WHERE nom_equipe = '" + equipe + "'");

            string idTeam = idEquipe.ElementAt(0)[0];

            req.request(@"INSERT INTO EQUIPE_CONFRONTATION
                        VALUES (1, NULL, '" + idTeam + "', '" + idConf + "' )");
        }

        public void ajouterEquipe2(string equipe, int idConf)
        {
            List<string[]> idEquipe = req.selectRequest(@"SELECT id_equip
                                                        FROM EQUIPE
                                                        WHERE nom_equipe = '" + equipe + "'");

            string idTeam = idEquipe.ElementAt(0)[0];

            req.request(@"INSERT INTO EQUIPE_CONFRONTATION
                        VALUES (0, NULL, '" + idTeam + "', '" + idConf + "' )");
        }

    }
}