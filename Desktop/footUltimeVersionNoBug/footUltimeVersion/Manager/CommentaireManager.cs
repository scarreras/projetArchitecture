﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projetfoot.Manager
{
    public class CommentaireManager
    {
        GestionDB.GestionDB req = new GestionDB.GestionDB();

        public List<string[]> recupererListeCommentaires(string id_art)
        {
            List<string[]> liste_commentaires = req.selectRequest(@"SELECT contenu_com, login_util
                                                        FROM COMMENTAIRE C, UTILISATEUR U
                                                        WHERE C.id_util = U.id_util
                                                        AND id_art = '" + id_art + "';");

            return liste_commentaires;
        }

        public void ajouterCommentaire(string commentaire, string idArticle)
        {

            req.request(@"INSERT INTO COMMENTAIRE
                        VALUES (NULL, '" + commentaire + "', 2, '" + idArticle + "')");
        }

        public List<string[]> recupererComEnAttenteDeValidation()
        {
            List<string[]> liste_commentaires = req.selectRequest(@"SELECT id_com,contenu_com, validation
                                                                    FROM commentaire WHERE validation = 0");



            return liste_commentaires;
        }

        /*Quand un commentaire est validé on update la valeur du booléen pour confirmer */
        public void valider_commentaire(int id_commentaire)
        {
            req.request(@"UPDATE commentaire SET validation = '1' WHERE commentaire.id_com = "+id_commentaire);  
        }

        /*Quand le commentaire n'a pas été validé on le supprime définitivement*/
        public void supprimer_commentaire(int id_commentaire)
        {
            req.request(@"DELETE FROM commentaire WHERE commentaire.id_com ="+id_commentaire);
        }


    }
}