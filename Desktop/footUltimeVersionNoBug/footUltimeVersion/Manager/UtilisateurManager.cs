﻿using footUltimeVersion.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projetfoot.Manager
{
    public class UtilisateurManager
    {
        GestionDB.GestionDB req = new GestionDB.GestionDB();

        public void ajouterUtilisateur(string pseudo, string nom, string prenom, string mdp, string mail, DateTime date_de_naissance)
        {
            req.request(@"INSERT INTO utilisateur
                          VALUES (NULL, '" + pseudo + "', '" + mdp + "', '" + nom + "', '" + prenom + "', '" + date_de_naissance.Date.ToString("yyyy-MM-dd") + "', '" + mail + "', '1');");
        }

        public bool connexionUtilisateur(string login, string mdp)
        {
            bool existence_utilisateur = req.checkRequest(@"SELECT id_util FROM utilisateur WHERE login_util = '"+login+"' AND mdp_util ='"+mdp+"'");

          
            return existence_utilisateur;

        }

        public string recupererIdUtil(string login)
        {
            List<string[]> id_utilisateur = req.selectRequest(@"Select id_util From utilisateur WHERE login_util = '"+login+"'");

            string id_util = id_utilisateur.ElementAt(0)[0];

            return id_util;
        }

        //On vérifie si l'utilisateur a les droits d'admin pour accéder au menu de saisie et de modification
        public bool verifierAdmin(string id_util)
        {
            bool est_admin = req.checkRequest(@"SELECT id_util FROM utilisateur WHERE id_util = '"+id_util+"' AND id_type = '1'");


            return est_admin;

        }

    }
}