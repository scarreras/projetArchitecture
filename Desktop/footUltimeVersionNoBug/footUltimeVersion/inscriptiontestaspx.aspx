﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Site.Master" CodeBehind="inscriptiontestaspx.aspx.cs" Inherits="footUltimeVersion.inscriptiontestaspx" %>
 <asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

     <link rel="stylesheet" href="Content/PageInscription.css"/>

		<div class="body body-s">
		
			<form  class="sky-form">
				<header><strong> Formulaire d'inscription</strong></header>
				
				<fieldset>					
					<section>
						<label class="input">
							<i class="icon-append icon-user"></i>
							<input type="text" placeholder="Pseudo"/>
							<b class="tooltip tooltip-bottom-right">Lettres et chiffres uniquement.</b>
						</label>
					</section>
					
					<section>
						<label class="input">
							<i class="icon-append icon-envelope-alt"></i>
							<input type="text" placeholder="Adresse Email"/>
							<b class="tooltip tooltip-bottom-right">Obligatoire afin de vérifier </b>
						</label>
					</section>
					
					<section>
						<label class="input">
							<i class="icon-append icon-lock"></i>
							<input type="password" placeholder="Mot de passe"/>
							<b class="tooltip tooltip-bottom-right">Lettres et chiffres uniquement. </b>
						</label>
					</section>
					
					<section>
						<label class="input">
							<i class="icon-append icon-lock"></i>
							<input type="password" placeholder="Répétez le mot de passe"/>
							<b class="tooltip tooltip-bottom-right">Identique au premier mot de passe renseigné. </b>
						</label>
					</section>
				</fieldset>
					
				<fieldset>
					<div class="row">
						<section class="col col-6">
							<label class="input">
								<input type="text" placeholder="Prénom"/>
							</label>
						</section>
						<section class="col col-6">
							<label class="input">
								<input type="text" placeholder="Nom"/>
							</label>
						</section>
					</div>
					
					<section>
						<label class="select">
							<select>
								<option value="0">Sexe</option>
								<option value="1">Homme</option>
								<option value="2">Femme</option>
								<option value="3">Autre</option>
							</select>
							<i></i>
						</label>
					</section>
					
				</fieldset>
				<footer>
					<button type="submit" class="button">Valider</button>
				</footer>
			</form>
			
		</div>
        </asp:Content>
